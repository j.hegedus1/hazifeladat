#include <stdio.h>
#include <stdlib.h>
#include <inetaddr1.h>

long long ipcimvizsgalo(char *s)
{
    int i=0;
    int maxszam =4;
    long long ipcimszamkent=0;
    char *szam = NULL;

    szam = (char*) strtok(s, ".");
    while(szam != NULL)
    {
        i++;
        if (atoi(szam)<0 || atoi(szam) > 255)
            return -1;
        ipcimszamkent<<=8;
        ipcimszamkent+= atoi(szam);
        szam = (char*) strtok(NULL, ".");
    }
    if (szam== NULL && i!=maxszam)
        return -2;

    return ipcimszamkent;
}

void inetaddr1(char *s)
{
    long long a =ipcimvizsgalo(s);
    switch(a)
    {
    case -1 :
        printf("\nNem valos IP cim. Csak 0 es 255 koze eso szamokat tartalmazhat!");
        break;
    case -2 :
        printf("\nNem valos IP cim. A cimnek pontosan negy szamot kell tartalmaznia!");
        break;

    default:
        printf("\n\n\nValodi IP cim!");
        printf("\nA cim szamkent:%lld", a);
    }
    return 0;
}
