#include <stdio.h>
#include <stdlib.h>
#include <emailvizsgalo.h>

#define hiba1 1   //Az eslo es utolso karakter nem lehet @ vagy . !
#define hiba2 2   //Nincs benne kukac!
#define hiba3 4   //Tul sok benne a kukac!
#define hiba4 8   //A kukac mellett nem lehet pont!
#define hiba5 16  //A kukac utan legalabb egy pontnak lennie kell!
#define hiba6 32  //A cim tul hosszu! (max. 256 karakter lehet)


int emailvizsgalo (char *email)
{
     int kukc=0,pont=0,i=0, hiba=0;

     while (email[i++])
        {
        if (email[i]== '@')
            {
            kukc++;
            if((email[i+1] == '.') || (email[i-1] == '.')) hiba |= hiba4;
            }
        if ((kukc > 0) && (email[i]== '.')) pont++;
        }
     if (email[0] == '@' || email[0] == '.' || email[i-2] == '@' || email[i-2] == '.') hiba |= hiba1;
     if (kukc == 0)hiba |= hiba2;
     if (kukc > 1) hiba |= hiba3;
     if (pont < 1) hiba |= hiba5;
     if (i > 256) hiba |= hiba6;
     return hiba;
}

 void emailvizsg( char* email)
 {
    int hiba=emailvizsgalo(email);

    if (hiba==0) printf("\n\n\nFormailag elfogadott email cim!(A felhaszalt karaktereket nem vizsgaltam:)");
        else printf("\n Ervenytelen cim. ");

    if (hiba&hiba1) printf("\n Az eslo es utolso karakter nem lehet @ vagy . !");
    if (hiba&hiba2) printf("\n Nincs benne kukac!");
    if (hiba&hiba3) printf("\n Tul sok benne a kukac!");
    if (hiba&hiba4) printf("\n A kukac mellett nem lehet pont!");
    if (hiba&hiba5) printf("\n A kukac utan legalabb egy pontnak lennie kell!");
    if (hiba&hiba6) printf("\n A cim tul hosszu! (max. 256 karakter lehet)");

    return 0;
}

