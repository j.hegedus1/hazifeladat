#include <stdio.h>
#include <stdlib.h>
#include <primkiiro.h>

int prim1(long long a)
{
    long long c=2;
    if (a==1||a==0) a=4;
    if (a==2) a=3;
    while (c*c < a+1)
        {
        if (a%c++ == 0) return 0;
        }
    return 1;
}

void primkiir(long long min, long long max)
{
    long long i;

    if (min==0 && max==0) return 1;
    if (min<0) min *= -1;
    if (max<0) max *= -1;
    if (min>max) {i=min;min=max;max=i;}

    printf("\n%lld es %lld kozotti primszamok:",min,max);

    while (min <= max) if (prim1(min++)) printf("%lld, ",min-1);

    return 0;
}
