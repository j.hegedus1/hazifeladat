#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <eratoszprim.h>
#include <math.h>

 long long eratosztenesziszita (long long min, long long max)
{
    if (min <2) min = 2;
    long long i5=0, i6=0, a=0, primekszama=0;
    long long maxgyoke = sqrt(max)+1;
    long long maxgyokegyoke = sqrt(maxgyoke)+1;

    char *primek = (char*) calloc(maxgyoke,sizeof(unsigned char));

    if (primek == NULL)
        {
        printf("Nem sikerult memoriat foglalni!\n");
        return -1;
        }

    primek[0]=1;primek[1]=1;            //maximum gy�k�ig l�trehozunk egy eatosztel�szi szit�t
    for (i5=2;i5<=maxgyokegyoke;i5++)
        if (primek[i5]==0)
            for (i6 = (i5*i5); i6 <= maxgyoke; i6 += i5 )
                primek[i6]=1;

   char *primek1 = (char*) calloc((max-min+1),sizeof(unsigned char));
    if (primek1 == NULL)
        {
        printf("Nem sikerult memoriat foglalni!\n");
        return -1;
        }

        for (i6 = 2; i6 <= maxgyoke; i6++)  //A l�trehozott szita seg�ts�g�vel
            if (primek[i6] == 0 )           //a minimum �s a maximum k�z�tt is l�trhozunk egy szit�t
                {
                if (i6>=min) a=2*i6;
                    else if (min%i6) a = min+i6-(min%i6);
                        else a = min;

                for (i5 = a; i5 <= max; i5 += i6)
                    primek1[i5-min] = 1;
                }
        for (i5 = min; i5 <= max; i5++)
            if (primek1[i5-min]==0)
                primekszama++;

    free(primek);
    free(primek1);
    return primekszama;
}

    void primvizsg1(long long min, long long max){
    clock_t t;
    long long i5;

    if (min==0 && max==0) return 1;
    if (min<0) min *= -1;
    if (max<0) max *= -1;
    if (min>max) {i5=min;min=max;max=i5;}
    t = clock();
    printf("\n%lld es %lld kozott: %lld db primszam van.",min,max,eratosztenesziszita(min,max));
    t = clock() - t;
    printf ("\nA muvelet: %d ms-ig tartott. (%f sec)\n",t,((float)t)/CLOCKS_PER_SEC);
    return ;
    }
