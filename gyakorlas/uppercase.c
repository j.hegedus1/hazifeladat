#include <stdio.h>
#include <stdlib.h>
#include <uppercase.h>

#define feladat1 1          //Mind kisbetu
#define feladat2 2          //Mind nagybetu
#define feladat3 3          //Felcser�l
#define kisbetumin 'a'    // a asc k�dja
#define kisbetumax 'z'    // z asc k�dja
#define nagybetumin 'A'   // A asc k�dja
#define nagybetumax 'Z'   // Z asc k�dja
#define kulonbseg 32

void atalakito( char *s, int c)
{
    int a = 0;
    do
    {
        if ((s[a] >= nagybetumin && s[a] <= nagybetumax) && c == feladat1)
            s[a]=s[a]|kulonbseg;
        if ((s[a] >= kisbetumin && s[a] <= kisbetumax) && c == feladat2)
            s[a]=s[a]&~kulonbseg;
        if (((s[a] >= nagybetumin && s[a] <= nagybetumax)||(s[a] >= kisbetumin && s[a] <= kisbetumax)) && c == feladat3)
            s[a]=s[a]^kulonbseg;
    }
    while(s[a++]);
    return;
}


void uppercase(char* s)
{
    atalakito(s,feladat3);
    printf("\n\nKicsit nagyra,nagyot kicsire(felcserel): %s",s);
    atalakito(s,feladat1);
    printf("\n\nMind kisbetu: %s",s);
    atalakito(s,feladat2);
    printf("\n\nMind nagybetu: %s",s);
    printf("\n\n");
    return 0;
}
