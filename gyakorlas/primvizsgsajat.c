#include <stdio.h>
#include <stdlib.h>
#include <primvizsgsajat.h>
#include <time.h>
#include <math.h>

int prim(long long a)       //primvizsg�lat egyszer� oszt�ssal
{
    long long c=2;
    if (a==1||a==0) a=4;
    if (a==2) a=3;
    while (c*c < a+1)
        {
        if (a%c++ == 0) return 0;
        }
    return 1;
}

long long primvizsgsajat(long long min, long long max)
 {
    long long i = 0, j = 2, primekszama = 0, tombmeret = 0;
    long long maxgyoke = sqrt(max)+1;

    while (j <= maxgyoke)
        if (prim(j++))
            tombmeret++;

    long long *primek = (long long*) calloc(tombmeret,sizeof(long long));
    if (primek == NULL) {
        printf("Nem sikerult memoriat foglalni!\n");
        return 1;}

    for (j=2; j <= maxgyoke;j++)       //l�trehozunk egy t�mb�t ami tartalmazza a maximum gy�k�ig a pr�meket
        if (prim (j))primek[primekszama++]=j;

    primekszama= 0;                    // A minimum �s a maximum k�z�tt a t�mb elemeivel leosztjuk a sz�mokat
    if (max==1){min=0;max=0;}
        for (j=min; j<= max; j++)
            {
            if (j==1)j++;
                for(i=0; i <= tombmeret-1;i++)
                    {
                    if (j%(primek[i])==0) break;
                    }
                if (j%(primek[i])!=0 || j==primek[i])primekszama++;
            }
        return primekszama;
}

  void primvizsg(long long min, long long max)
{
    clock_t t;
    long long i=0;
    if (min==0 && max==0) return 1;
    if (min<0) min *= -1;
    if (max<0) max *= -1;
    if (min>max) {i=min;min=max;max=i;}

    t = clock();
    printf("\n%lld es %lld kozott: %lld db primszam van.",min,max,primvizsgsajat(min,max));
    t = clock() - t;
    printf ("\nA muvelet: %d ms-ig tartott. (%f sec)\n",t,((float)t)/CLOCKS_PER_SEC);
    return ;
}

