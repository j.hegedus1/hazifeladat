#include <stdio.h>
#include <stdlib.h>
#include <emailvizsgalo.c>
#include <uppercase.c>
#include <eratoszprim.c>
#include <inetaddr1.c>
#include <primvizsgsajat.c>
#include <primkiiro.c>

//Készítette: Hegedüs József (VVZKYG)

void hiba()
{
    printf("Nem megfelelo parameterek!!!");
    printf("\n\n1. Primvizsgalo. Megszamolja hogy A es B kozott hany prim talalhato! \n(gyakorlas.exe 1 A B)");
    printf("\n\n2. Emailcim validalo. Megvizsgalja, hogy a megadott string lehet-e email cim! \n(gyakorlas.exe 2 string)");
    printf("\n\n3. Uppercase(). A megadott stringben szereplo betuket:");
    printf("\n\t1. Kis es nagybetut felcserel!\n\t2. Mindet kisbeture allitja!\n\t3. Mindet nagybeture allitja!\n(gyakorlas.exe 3 string)");
    printf("\n\n4. Primkiiro. Kiirja hogy A es B kozott mely szamok primek! \n(gyakorlas.exe 4 A B)");
    printf("\n\n5. Primvizsgalo. Megszamolja hogy A es B kozott hany prim talalhato!(eratoszteleszi szita) \n(gyakorlas.exe 5 A B)");
    printf("\n\n6. Inetaddr. Megvizsgalja, hogy a megadott string valos IP cim-e. \n(gyakorlas.exe 6 xxx.xxx.xxx.xxx)");
    printf("\n\nViszont latasra!!!");
    return 121;
}
int main(int argc, char* argv[])
{
   switch(atoi(argv[1]))
    {
    case 1 : if (argc < 3) {hiba();break;} else primvizsg(atoll(argv[2]), atoll(argv[3]));break;
    case 2 : if (argc < 3) {hiba();break;} else emailvizsg(argv[2]);break;
    case 3 : if (argc < 3) {hiba();break;} else uppercase(argv[2]);break;
    case 4 : if (argc < 3) {hiba();break;} else primkiir(atoll(argv[2]), atoll(argv[3]));break;
    case 5 : if (argc < 3) {hiba();break;} else primvizsg1(atoll(argv[2]), atoll(argv[3]));break;
    case 6 : if (argc < 3) {hiba();break;} else inetaddr1(argv[2]);break;
    default: hiba();
    }
    return;
}

